package com.example.springhomeworkrestapiwithmybatis02.service.serviceImp;

import com.example.springhomeworkrestapiwithmybatis02.model.enitty.Product;
import com.example.springhomeworkrestapiwithmybatis02.model.request.ProductRequest;
import com.example.springhomeworkrestapiwithmybatis02.repository.ProductRepository;
import com.example.springhomeworkrestapiwithmybatis02.service.ProductService;
import org.springframework.stereotype.Service;

import java.util.List;


@Service
public class ProductServiceImp implements ProductService {

  private final ProductRepository productRepository;

    public ProductServiceImp(ProductRepository productRepository) {
        this.productRepository = productRepository;
    }

    @Override
    public List<Product> getAllProducts() {
        return productRepository.getAllProducts();
    }

    @Override
    public Product getProductById(Integer productId) {
        return productRepository.getProductById(productId);
    }

    @Override
    public boolean deleteProductById(Integer productId) {

        return productRepository.deleteProductById(productId);
    }

    @Override
    public Integer addNewCustomer(ProductRequest productRequest) {
        return null;
    }

    @Override
    public Product getAllProductById(Integer productId) {
        return null;
    }
}
