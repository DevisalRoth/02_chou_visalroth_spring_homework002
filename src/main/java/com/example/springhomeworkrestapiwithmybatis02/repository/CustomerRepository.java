package com.example.springhomeworkrestapiwithmybatis02.repository;

import com.example.springhomeworkrestapiwithmybatis02.model.enitty.Customer;
import com.example.springhomeworkrestapiwithmybatis02.model.request.CustomerRequest;
import org.apache.ibatis.annotations.*;

import java.util.List;

@Mapper
public interface CustomerRepository {
    @Select("SELECT * FROM customer_tb")
    List<Customer> findAllCustomer();


    @Select("SELECT * FROM customer_tb WHERE customer_id = #{customer_id}")
    Customer getCustomerById(Integer customerId);

    @Delete("DELETE FROM customer_tb WHERE customer_id = #{customer_id}")
    boolean deleteCustomerById(@Param("customer_id") Integer customerId);


    @Select("INSERT INTO customer_tb (customer_name, customer_address, customer_phone) " +
            "VALUES(#{request.customer_name} ,#{request.customer_address}, #{request.customer_phone})" +
            "RETURNING  customer_id")
    Integer saveCustomer(@Param("request") CustomerRequest customerRequest);

//    @Select("UPDATE customer_tb " +
//            "SET customer_name = #{request.customer_name}," +
//            "customer_address = #{request.customer_address}," +
//            "customer_phone = #{request.customer_phone} " +
//            "WHERE customer_id = #{customerId} " +
//            "RETURNING customer_id")
    @Select("""
            UPDATE customer_tb 
            SET customer_name = #{request.customer_name},
            customer_address = #{request.customer_address},
            customer_phone = #{request.customer_phone} 
            WHERE customer_id = #{customerId} 
            RETURNING customer_id
            """)
    Integer updateCustomer(@Param("request") CustomerRequest customerRequest, Integer customerId);



}