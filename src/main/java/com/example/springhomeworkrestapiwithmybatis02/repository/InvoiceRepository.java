package com.example.springhomeworkrestapiwithmybatis02.repository;


import com.example.springhomeworkrestapiwithmybatis02.model.enitty.Invoice;
import com.example.springhomeworkrestapiwithmybatis02.model.request.InvoiceRequest;
import org.apache.ibatis.annotations.*;

import java.util.List;

@Mapper
public interface InvoiceRepository {


    @Select("SELECT *FROM invoice_tb " +
            "LIMIT #{size} " +
            "OFFSET ${size} * (#{page} - 1)")
    @Results(
            id = "invoiceMapper",
            value ={
            @Result(property = "invoice_id" , column = "invoice_id"),
            @Result(property ="customer"  ,column = "customer_id",
                    one = @One(select = "com.example.springhomeworkrestapiwithmybatis02.repository.CustomerRepository.getCustomerById")
            ),

            @Result(property = "productList" ,column = "invoice_id",
                    many = @Many(select = "com.example.springhomeworkrestapiwithmybatis02.repository.ProductRepository.getProductByInvoiceId")
            )
    }
    )

    List<Invoice> findAllInvoice(Integer page, Integer size);
    @Select("SELECT *FROM invoice_tb WHERE invoice_id =#{invoice_id}")
    @ResultMap("invoiceMapper")
    Invoice getAllInvoice(Integer invoiceId);
    Invoice getInvoiceById(Integer invoiceId);
//    List<Invoice> findInvoiceById();
//    @Select("SELECT *FROM invoice_tb WHERE invoice_id =#{invoice_id}")
//    @ResultMap("invoiceMapper")
//    Invoice getInvoiceById(Integer invoiceId);

    @Select("INSERT INTO invoice_tb( invoice_id, invoice_date ) " +
            "VALUES (#{request.invoice_id}, #{request.invoice_date} " +
            "RETURNING invoice_id")
    Integer saveInvoice(@Param("request") InvoiceRequest invoiceRequest);


    @Select("INSERT INTO  invoice_detail(invoice_id,product_id ) " +
            "VALUES (#{invoiceId} ,#{productId})")
    Integer saveProductById(Integer invoiceId, Integer productId );


}

