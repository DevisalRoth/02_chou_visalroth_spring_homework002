package com.example.springhomeworkrestapiwithmybatis02.controller;

import com.example.springhomeworkrestapiwithmybatis02.model.enitty.Customer;
import com.example.springhomeworkrestapiwithmybatis02.model.request.CustomerRequest;
import com.example.springhomeworkrestapiwithmybatis02.model.response.CustomerResponse;
import com.example.springhomeworkrestapiwithmybatis02.repository.CustomerRepository;
import com.example.springhomeworkrestapiwithmybatis02.service.CustomerService;
import io.swagger.v3.oas.annotations.Operation;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import java.sql.Timestamp;
import java.util.List;

@RestController
@RequestMapping("/api/v1/customer")
public class CustomerController {


    private final CustomerService customerService;

    public CustomerController(CustomerService customerService) {
        this.customerService = customerService;
    }

    @GetMapping("/all")
    @Operation(summary = "Get all customer")
    public ResponseEntity<CustomerResponse<List<Customer>>> getAllCustomer(){
        CustomerResponse<List<Customer>> response = CustomerResponse.<List<Customer>>builder()
                .message("Fetched data successfully")
                .payload(customerService.getAllCustomer())
                .httpStatus(HttpStatus.OK)
                .timestamp(new Timestamp(System.currentTimeMillis()))
                .build();

        return ResponseEntity.ok(response);
    }



    @GetMapping("/{id}")
    @Operation(summary = "Get customer by id")
    public ResponseEntity<CustomerResponse<Customer>> getCustomerById(@PathVariable("id") Integer customerId){

        CustomerResponse<Customer> response =null;
        if(customerService.getAllCustomerById(customerId) != null){
             response = CustomerResponse.<Customer>builder()
                    .message("Success fetch data by id")
                    .payload(customerService.getAllCustomerById(customerId))
                    .httpStatus(HttpStatus.OK)
                    .timestamp(new Timestamp(System.currentTimeMillis()))
                    .build();
            return ResponseEntity.ok(response);
        }
        else{
            response = CustomerResponse.<Customer>builder()
                    .message("Data not found")
                    .httpStatus(HttpStatus.NOT_FOUND)
                    .timestamp(new Timestamp(System.currentTimeMillis()))
                    .build();
            return ResponseEntity.badRequest().body(response);
        }
    }

    @DeleteMapping("/{customer_id}")
    @Operation(summary = "Delete customer by id")
    public ResponseEntity<CustomerResponse<String>> deleteCustomerById(@PathVariable("customer_id") Integer customerId){

        CustomerResponse<String> response = null;
        if(customerService.deleteCustomerById(customerId)==true){
            response = CustomerResponse.<String>builder()
                    .message("Delete successfully")
                    .httpStatus(HttpStatus.OK)
                    .timestamp(new Timestamp(System.currentTimeMillis()))
                    .build();
        }
        return ResponseEntity.ok(response);
    }

    @PostMapping
    @Operation(summary = "Save new customer")
    public ResponseEntity<CustomerResponse<Customer>> addNewCustomer(@RequestBody CustomerRequest customerRequest){
        Integer storeCustomerId = customerService.addNewCustomer(customerRequest);
        if(storeCustomerId!=null){
            CustomerResponse<Customer> response = CustomerResponse.<Customer>builder()
                    .message("Add success")
                    .payload(customerService.getAllCustomerById(storeCustomerId))
                    .httpStatus(HttpStatus.OK)
                    .timestamp(new Timestamp(System.currentTimeMillis()))
                    .build();
            return ResponseEntity.ok(response);
        }
        return null;
    }

    @PutMapping("/{customer_id}")
    @Operation(summary = "Update customer by Id")
    public ResponseEntity<CustomerResponse<Customer>> updateCustomerById (
            @RequestBody CustomerRequest customerRequest, @PathVariable("customer_id") Integer customerId
    ){
        CustomerResponse<Customer> response = null;
        Integer idCustomerUpdate = customerService.updateCustomer(customerRequest,customerId);
        if(idCustomerUpdate != null){
            response = CustomerResponse.<Customer>builder()
                    .message("Update Sucessfully")
                    .payload(customerService.getAllCustomerById(idCustomerUpdate))
                    .httpStatus(HttpStatus.OK)
                    .timestamp(new Timestamp(System.currentTimeMillis()))
                    .build();
        }
        return ResponseEntity.ok(response);
    }
}
