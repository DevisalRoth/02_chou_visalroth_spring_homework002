package com.example.springhomeworkrestapiwithmybatis02.model.response;

import com.example.springhomeworkrestapiwithmybatis02.model.enitty.Customer;
import com.example.springhomeworkrestapiwithmybatis02.model.enitty.Product;
import com.fasterxml.jackson.annotation.JsonInclude;
import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;
import org.springframework.http.HttpStatus;

import java.sql.Timestamp;
import java.util.ArrayList;
import java.util.List;


@Data
@AllArgsConstructor
@NoArgsConstructor
@Builder
public class InvoiceResponse <T>{
    private String message;
    @JsonInclude(JsonInclude.Include.NON_NULL)
    private T payload;
    private HttpStatus httpStatus;
    private Timestamp timestamp;
    private Customer customer;

}
