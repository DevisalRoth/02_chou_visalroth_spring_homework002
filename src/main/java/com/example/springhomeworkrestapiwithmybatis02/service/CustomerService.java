package com.example.springhomeworkrestapiwithmybatis02.service;

import com.example.springhomeworkrestapiwithmybatis02.model.enitty.Customer;
import com.example.springhomeworkrestapiwithmybatis02.model.enitty.Product;
import com.example.springhomeworkrestapiwithmybatis02.model.request.CustomerRequest;

import java.util.List;

public interface CustomerService {
    List<Customer> getAllCustomer();
    Customer getAllCustomerById(Integer customerId);

    boolean deleteCustomerById(Integer customerId);



    Integer addNewCustomer(CustomerRequest customerRequest);

    Integer updateCustomer(CustomerRequest customerRequest, Integer customerId);
}
