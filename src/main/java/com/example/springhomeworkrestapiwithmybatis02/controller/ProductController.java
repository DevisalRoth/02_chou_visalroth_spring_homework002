package com.example.springhomeworkrestapiwithmybatis02.controller;


import com.example.springhomeworkrestapiwithmybatis02.model.enitty.Customer;
import com.example.springhomeworkrestapiwithmybatis02.model.enitty.Product;
import com.example.springhomeworkrestapiwithmybatis02.model.request.CustomerRequest;
import com.example.springhomeworkrestapiwithmybatis02.model.request.ProductRequest;
import com.example.springhomeworkrestapiwithmybatis02.model.response.CustomerResponse;
import com.example.springhomeworkrestapiwithmybatis02.model.response.ProductResponse;
import com.example.springhomeworkrestapiwithmybatis02.service.ProductService;
import io.swagger.v3.oas.annotations.Operation;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import java.sql.Timestamp;
import java.util.List;

@RestController
@RequestMapping("/api.v1/product")
public class ProductController {

    private final ProductService productService;

    public ProductController(ProductService productService) {
        this.productService = productService;
    }

    @GetMapping("/all")
    @Operation(summary = "Get all products")
    public ResponseEntity<ProductResponse<List<Product>>> getAllProduct(){
        ProductResponse<List<Product>> response = ProductResponse.<List<Product>>builder()
                .message("Get product successfully")
                .payload(productService.getAllProducts())
                .httpStatus(HttpStatus.OK)
                .timestamp(new Timestamp(System.currentTimeMillis()))
                .build();

        return ResponseEntity.ok(response);
    }

    @GetMapping("/{product_id}")
    @Operation(summary = "Get product by id")
    public ResponseEntity<ProductResponse<Product>> getProductById(@PathVariable("product_id") Integer productId){

        ProductResponse<Product> response = ProductResponse.<Product>builder()
                .message("Get product successfully")
                .payload(productService.getProductById(productId))
                .httpStatus(HttpStatus.OK)
                .build();
        return ResponseEntity.ok(response);
    }

    @DeleteMapping("/{product_id}")
    @Operation(summary = "Delete product by id")

    public ResponseEntity<ProductResponse<String>> deleteProductById(@PathVariable("product_id") Integer productId){

        ProductResponse<String> response = null;
        if(productService.deleteProductById(productId)==true){
            response = ProductResponse.<String>builder()
                    .message("Delete successfully")
                    .httpStatus(HttpStatus.OK)
                    .timestamp(new Timestamp(System.currentTimeMillis()))
                    .build();
        }
        return ResponseEntity.ok(response);
    }



}
