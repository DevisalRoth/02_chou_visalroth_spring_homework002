package com.example.springhomeworkrestapiwithmybatis02.service.serviceImp;

import com.example.springhomeworkrestapiwithmybatis02.model.enitty.Customer;
import com.example.springhomeworkrestapiwithmybatis02.model.enitty.Product;
import com.example.springhomeworkrestapiwithmybatis02.model.request.CustomerRequest;
import com.example.springhomeworkrestapiwithmybatis02.repository.CustomerRepository;
import com.example.springhomeworkrestapiwithmybatis02.service.CustomerService;
import org.springframework.stereotype.Service;
import java.util.List;

@Service
public class CustomerServiceImp implements CustomerService{

    private final CustomerRepository customerRepository;

    public CustomerServiceImp(CustomerRepository customerRepository) {
        this.customerRepository = customerRepository;
    }

    @Override
    public List<Customer> getAllCustomer() {
        return customerRepository.findAllCustomer();
    }

    @Override
    public Customer getAllCustomerById(Integer authorId) {
        return customerRepository.getCustomerById(authorId);
    }

    @Override
    public boolean deleteCustomerById(Integer customerId) {
        return customerRepository.deleteCustomerById(customerId);
    }



    @Override
    public Integer addNewCustomer(CustomerRequest customerRequest) {
        Integer customerId =customerRepository.saveCustomer(customerRequest);
        return customerId;
    }

    @Override
    public Integer updateCustomer(CustomerRequest customerRequest, Integer customerId) {
        Integer customerIdUpdate = customerRepository.updateCustomer(customerRequest,customerId);

        return customerIdUpdate;
    }
}
