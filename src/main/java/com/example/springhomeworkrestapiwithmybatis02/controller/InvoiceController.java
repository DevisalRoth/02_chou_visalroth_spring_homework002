package com.example.springhomeworkrestapiwithmybatis02.controller;


import com.example.springhomeworkrestapiwithmybatis02.model.enitty.Invoice;
import com.example.springhomeworkrestapiwithmybatis02.model.request.InvoiceRequest;
import com.example.springhomeworkrestapiwithmybatis02.model.response.InvoiceResponse;
import com.example.springhomeworkrestapiwithmybatis02.service.InvoiceService;
import io.swagger.v3.oas.annotations.Operation;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import java.sql.Timestamp;
import java.util.List;

@RestController
@RequestMapping("/api/v1/invoice_tb")
public class InvoiceController {
    private final InvoiceService invoiceService;

    public InvoiceController(InvoiceService invoiceService) {
        this.invoiceService = invoiceService;
    }


    @GetMapping("/all")
    @Operation(summary = "Get all Invoice")
    public ResponseEntity<InvoiceResponse<List<Invoice>>> getAllInvoice(@RequestParam Integer page , @RequestParam Integer size){

        InvoiceResponse<List<Invoice>> response =null;

        if (invoiceService.getAllInvoice(page,size)!=null){
            response = InvoiceResponse.<List<Invoice>>builder()
                    .message("Get Invoice successfully")
                    .payload(invoiceService.getAllInvoice(page, size))
                    .httpStatus(HttpStatus.OK)
                    .timestamp(new Timestamp(System.currentTimeMillis()))
                    .build();
        }
        return ResponseEntity.ok(response);
    }


    @GetMapping("/{invoice_id}")
    @Operation(summary = "Get all Invoice")
    public ResponseEntity<Invoice> getAllInvoice(@PathVariable("invoice_id") Integer invoiceId){

        return ResponseEntity.ok(invoiceService.getInvoiceById(invoiceId));
    }

//    @GetMapping("/invoice_id")
//    @Operation(summary = "Get Invoice by id")
//    public ResponseEntity<InvoiceResponse<Invoice>> getInvoiceById(@PathVariable Integer invoiceId){
//
//        InvoiceResponse<Invoice> response =null;
//
//        if (invoiceService.getInvoiceById(invoiceId)!=null){
//            response = InvoiceResponse.<Invoice>builder()
//                    .message("Get Invoice successfully")
//                    .payload(invoiceService.getInvoiceById(invoiceId))
//                    .httpStatus(HttpStatus.OK)
//                    .timestamp(new Timestamp(System.currentTimeMillis()))
//                    .build();
//        }
//        return ResponseEntity.ok(response);
//    }

    @PostMapping("/save")
    @Operation(summary = "Add new invoice")
    public ResponseEntity<InvoiceResponse<Invoice>> addNewInvoice(@RequestBody InvoiceRequest invoiceRequest){

        InvoiceResponse<Invoice> response = null;
        Integer storeInvoiceId = invoiceService.addNewInvoice(invoiceRequest);
        if(storeInvoiceId !=null){
            response =  InvoiceResponse.<Invoice>builder()
                    .message("Success for post Invoice")
                    .payload(invoiceService.getInvoiceById(storeInvoiceId))
                    .httpStatus(HttpStatus.OK)
                    .build();
            return ResponseEntity.ok(response);
        }
        return null;

    }

}
