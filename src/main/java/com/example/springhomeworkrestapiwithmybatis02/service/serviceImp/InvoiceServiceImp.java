package com.example.springhomeworkrestapiwithmybatis02.service.serviceImp;

import com.example.springhomeworkrestapiwithmybatis02.model.enitty.Invoice;
import com.example.springhomeworkrestapiwithmybatis02.model.enitty.Product;
import com.example.springhomeworkrestapiwithmybatis02.model.request.InvoiceRequest;
import com.example.springhomeworkrestapiwithmybatis02.repository.InvoiceRepository;
import com.example.springhomeworkrestapiwithmybatis02.service.InvoiceService;
import org.springframework.stereotype.Service;

import java.util.List;


@Service
public class InvoiceServiceImp implements InvoiceService {
    private final InvoiceRepository invoiceRepository;

    public InvoiceServiceImp(InvoiceRepository invoiceRepository) {
        this.invoiceRepository = invoiceRepository;
    }


    @Override
    public List<Invoice> getAllInvoice(Integer page, Integer size) {
        return invoiceRepository.findAllInvoice(page,size);
    }

    @Override
    public Invoice getInvoiceById(Integer invoiceId) {
        return invoiceRepository.getInvoiceById(invoiceId);
    }

    @Override
    public Integer addNewInvoice(InvoiceRequest invoiceRequest) {
        Integer storeInvoiceId = invoiceRepository.saveInvoice(invoiceRequest);

        for(Product productId :invoiceRequest.getProduct_id()){
            invoiceRepository.saveProductById(storeInvoiceId, productId.getProduct_id());
        }
        return storeInvoiceId;

    }



}
