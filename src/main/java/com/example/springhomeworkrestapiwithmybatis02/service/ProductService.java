package com.example.springhomeworkrestapiwithmybatis02.service;

import com.example.springhomeworkrestapiwithmybatis02.model.enitty.Product;
import com.example.springhomeworkrestapiwithmybatis02.model.request.CustomerRequest;
import com.example.springhomeworkrestapiwithmybatis02.model.request.ProductRequest;

import java.util.List;

public interface ProductService {

    List<Product> getAllProducts();
    Product getProductById(Integer productId);

    boolean deleteProductById(Integer productId);
    Integer addNewCustomer(ProductRequest productRequest);
    Product getAllProductById(Integer productId);
}
