package com.example.springhomeworkrestapiwithmybatis02.model.request;

import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;

@Data
@AllArgsConstructor
@NoArgsConstructor
@Builder
public class ProductRequest {
    private Integer product_id;
    private String product_name;
    private double product_price;


}
