package com.example.springhomeworkrestapiwithmybatis02.service;

import com.example.springhomeworkrestapiwithmybatis02.model.enitty.Invoice;
import com.example.springhomeworkrestapiwithmybatis02.model.request.InvoiceRequest;

import java.util.List;

public interface InvoiceService {
    List<Invoice> getAllInvoice(Integer page, Integer size);
    Invoice getInvoiceById(Integer invoiceId);

    Integer addNewInvoice(InvoiceRequest invoiceRequest );

}
