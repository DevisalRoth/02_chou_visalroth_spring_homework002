package com.example.springhomeworkrestapiwithmybatis02.model.enitty;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

import java.sql.Timestamp;
import java.util.ArrayList;
import java.util.List;

@Data
@AllArgsConstructor
@NoArgsConstructor
public class Invoice {
    private Integer invoice_id;
    private Timestamp invoice_date;
//    private Integer customer_id;
    private Customer customer;
    private List<Product> productList = new ArrayList<Product>();
}
