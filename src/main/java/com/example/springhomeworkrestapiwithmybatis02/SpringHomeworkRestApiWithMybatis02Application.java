package com.example.springhomeworkrestapiwithmybatis02;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class SpringHomeworkRestApiWithMybatis02Application {

    public static void main(String[] args) {
        SpringApplication.run(SpringHomeworkRestApiWithMybatis02Application.class, args);
    }

}
