package com.example.springhomeworkrestapiwithmybatis02.repository;
import com.example.springhomeworkrestapiwithmybatis02.model.enitty.Product;
import com.example.springhomeworkrestapiwithmybatis02.model.request.CustomerRequest;
import com.example.springhomeworkrestapiwithmybatis02.model.request.ProductRequest;
import org.apache.ibatis.annotations.Delete;
import org.apache.ibatis.annotations.Mapper;
import org.apache.ibatis.annotations.Param;
import org.apache.ibatis.annotations.Select;

import java.util.List;

@Mapper
public interface ProductRepository {


    @Select("SELECT * FROM product_tb")
    List<Product> getAllProducts();


    @Select("SELECT * FROM product_tb  WHERE product_id = #{productId}")
    Product getProductById (Integer productId);

    @Delete("DELETE FROM product_tb WHERE  product_id = #{product_id}")
    boolean deleteProductById(@Param("product_id") Integer productId);

    @Select("INSERT INTO product_tb (product_name, product_price ) " +
            "VALUES(#{request.product_name} ,#{request.product_price})" +
            "RETURNING  product_id")
    Integer saveProduct(@Param("request") ProductRequest productRequest);

    @Select("SELECT p.product_id , p.product_name, p.product_price FROM invoice_detail ip " +
            "INNER JOIN product_tb p on p.product_id = ip.product_id " +
            "WHERE ip.invoice_id = #{invoice_id}")
    List<Product> getProductByInvoiceId(Integer invoiceId);


}
