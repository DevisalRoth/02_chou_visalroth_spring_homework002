package com.example.springhomeworkrestapiwithmybatis02.model.request;


import com.example.springhomeworkrestapiwithmybatis02.model.enitty.Product;
import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;

import java.sql.Timestamp;
import java.util.List;

@Data
@AllArgsConstructor
@NoArgsConstructor
@Builder
public class InvoiceRequest {

    private Integer customer_id;
    private Timestamp invoice_date;
    private List<Product> product_id;
}
