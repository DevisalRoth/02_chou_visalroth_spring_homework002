package com.example.springhomeworkrestapiwithmybatis02.model.enitty;

import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;

@Data
@AllArgsConstructor
@NoArgsConstructor
@Builder
public class Customer {
    private Integer customer_id;
    private String customer_name;
    private String customer_address;
    private String customer_phone;
}
